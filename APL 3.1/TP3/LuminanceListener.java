import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class LuminanceListener implements MouseListener {

    private PolygonCreator polygonCreator;
    private List<Polygon> polygonList;

    public LuminanceListener(PolygonCreator polyCrea) {
        this.polygonCreator = polyCrea;
        this.polygonList = polygonCreator.getPolygonList();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        for (int i = 0; i < polygonList.size(); i++) {
            if (polygonList.get(i).contains(e.getPoint().getX(), e.getPoint().getY()-35)) {     // 35 is the height of the window title
                // System.out.println("i = " + i);
                // System.out.println("X = " + e.getPoint().getX() + "; Y = " + (e.getPoint().getY()-35));
                polygonCreator.translatePolygons(i);

                break;
            }
        }
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }
    
}
