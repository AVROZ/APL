import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.*;

public class PolygonCreator extends JComponent {

    static final int width = 40;
    static final int hight = 150;
    static final int marge = 10;
    static final int x = 5;

    List<Polygon> polyList;
    List<Color> colorList;

    Random rand;

    public PolygonCreator() {
        polyList = new ArrayList<>();
        colorList = new ArrayList<>();

        rand = new Random();

        for (int i = 0; i < 10; i++) {
            colorList.add(new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)));
        }

        int[] xPoints = {x, x+width, x+width*2, x+width};
        int[] yPoints = {hight-marge, hight-marge, marge, marge};

        polyList.add(new Polygon(xPoints, yPoints, 4));
        
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < xPoints.length; j++) {
                xPoints[j] = xPoints[j] + width + 5;
            }
            polyList.add(new Polygon(xPoints, yPoints, 4));
        }
    }

    @Override
    protected void paintComponent(Graphics pinceau) {
        Graphics secondPinceau = pinceau.create();

        if (this.isOpaque()) {
            secondPinceau.setColor(this.getBackground());
            secondPinceau.fillRect(0, 0, this.getWidth(), this.getHeight());
        }

        for (int i = 0; i < polyList.size(); i++) {
            secondPinceau.setColor(colorList.get(i));
            secondPinceau.fillPolygon(polyList.get(i));
        }
    }

    public void translatePolygons(int n) {
        for (int i = n+1; i < polyList.size(); i++) {
            for (int j = 0; j < polyList.get(i).xpoints.length; j++) {
                polyList.get(i).xpoints[j] -= width + 5;
            }
        }

        polyList.remove(n);
        colorList.remove(n);

        repaint();
    }

    public List<Polygon> getPolygonList() {
        return polyList;
    }
}