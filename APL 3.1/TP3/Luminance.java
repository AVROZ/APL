import java.awt.*;
import javax.swing.*;

public class Luminance {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(700, 200);
        frame.setLocation(100, 100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(1,1));

        PolygonCreator polyCrea = new PolygonCreator();

        polyCrea.setPreferredSize(new Dimension(100, 110));
        frame.add(polyCrea);
        frame.addMouseListener(new LuminanceListener(polyCrea));

        frame.setVisible(true);
    }
}