import java.util.ArrayList;

public class Liste {
    public static void main(String[] args) {
        ArrayList<Integer> intList = new ArrayList<>();
        ArrayList<Float> floatList = new ArrayList<>();
        ArrayList<Number> numberList = new ArrayList<>();

        intList.add(new Integer(1));
        // intList.add(new Float(1.2));
        // intList.add(new Long(12));

        // floatList.add(new Integer(1));
        floatList.add(new Float(1.2));
        // floatList.add(new Long(12));

        numberList.add(new Integer(1));
        numberList.add(new Float(1.2));
        numberList.add(new Long(12));

        numberList.addAll(intList);
        numberList.addAll(floatList);
    }
}