import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;

public class Tableaux {
    public static void main(String[] args) {
        //System.out.println(Arrays.toString(args));
        //System.out.println(Arrays.toString(Arrays.copyOf(args, 3)));

        Comparator<Object> comp = Collator.getInstance();
        Arrays.sort(args, comp);
        System.out.println(Arrays.toString(args));
    }
}