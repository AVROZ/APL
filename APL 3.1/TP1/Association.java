public class Association<E> {
    private E element;
    private int frequence;

    public Association(E element, int frequence) {
        this.element = element;
        this.frequence = frequence;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E elt) {
        this.element = elt;
    }

    public int getFrequence() {
        return frequence;
    }

    public void setFrequence(int frequence) {
        this.frequence = frequence;
    }

    public String toString() {
        return "L'élément " + element + " est apparu " + frequence + " fois";
    }
}
