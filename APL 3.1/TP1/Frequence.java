import java.util.Arrays;

public class Frequence {
    public static void main(String[] args) {
        Integer[] arrayOfInt = {10, 20, 20, 10, 10, 20, 5, 20};
        String[] arrayOfString = {"a", "a", "c", "c", "b", "d", "a"};

        System.out.println(mostRepeated(arrayOfInt));
        System.out.println(mostRepeated(arrayOfString));
    }

    public static <T> T mostRepeated(T[] arr) {
        int n = arr.length;
        boolean visited[] = new boolean[n];
        int most = 0;
        int indexOfMostRepeatedElement = 0;
     
        Arrays.fill(visited, false);
    
        // Traverse through array elements and
        // count frequencies
        for (int i = 0; i < n; i++) {
    
            // Skip this element if already processed
            if (visited[i] == true)
                continue;
    
            // Count frequency
            int count = 1;
            for (int j = i + 1; j < n; j++) {
                if (arr[i].equals(arr[j])) {
                    visited[j] = true;
                    count++;
                }
            }

            if (count > most) {
                most = count;
                indexOfMostRepeatedElement = i;
            }
        }

        return arr[indexOfMostRepeatedElement];
    }
}
