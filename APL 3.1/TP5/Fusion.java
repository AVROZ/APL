import java.util.ArrayDeque;

public class Fusion {

    private static int numberOfArgs;
    private static int c;

    public static void main(String[] args) {
        numberOfArgs = args.length;
        ArrayDeque<Float> mainFile = new ArrayDeque<>();
        ArrayDeque<Float> file1 = new ArrayDeque<>();
        ArrayDeque<Float> file2 = new ArrayDeque<>();

        for (int i = 0; i < numberOfArgs; i++) {
            file1.add(Float.valueOf(args[i]));
        }
        
        trier(mainFile, file1, file2);

        System.out.println(mainFile);
    }

    private static void trier(ArrayDeque<Float> mainFile, ArrayDeque<Float> file1, ArrayDeque<Float> file2) {
        scinder(file1, file2);

        fusionner(mainFile, file1, file2);
    }
    
    private static void scinder(ArrayDeque<Float> file1, ArrayDeque<Float> file2) {
        for (int i = 0; i < numberOfArgs/2; i++) {
            file2.add(file1.remove());
        }
    }

    private static void fusionner(ArrayDeque<Float> mainFile, ArrayDeque<Float> file1, ArrayDeque<Float> file2) {
        System.out.println(file1);
        System.out.println(file2);
        for (int i = 0; i < numberOfArgs/2; i++) {
            if (file1.isEmpty()) {
                mainFile.add(file2.remove());
            }
            if (file2.isEmpty()) {
                mainFile.add(file1.remove());
            }
            System.out.println(file1);
            System.out.println(file2);

            c = file1.remove().compareTo(file2.remove());
            if (c <= 0) {
                mainFile.add(file1.remove());
            }
            if (c > 0) {
                mainFile.add(file2.remove());
            }
        }
    }
}
