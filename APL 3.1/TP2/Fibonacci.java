public class Fibonacci {
	public static void main(String[] args) {
		System.out.println("Le i-ème termede la suite de fibonacci est : " + fibonacci(Integer.parseInt(args[0])));
	}

	public static long fibonacci(long n) {
		if ((n == 0) || (n == 1))
		   return n;
		else
		   return fibonacci(n - 1) + fibonacci(n - 2);
	 }
}
