public class Appels {
    private static int indentation;

    public static void main(String[] args) {
        int result;
        result = factorielle(Integer.parseInt(args[0]));

        System.out.println(result);
    }

    public static int factorielle(int number) {
        int result;

        if (indentation > 0) {
            for (int i = 0; i < indentation; i++) {
                System.out.print("    ");
            }
        }

        System.out.println("Avant : " + number);
        if (number > 1) {
            indentation++;
            result = number*factorielle(number-1);

            for (int i = 0; i < indentation; i++) {
                System.out.print("    ");
            }
            
            System.out.println("Après : " + result);
            return result;
        } else {
            return 1;
        }
    }
}