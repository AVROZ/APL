public class Tableaux {
    private static int i = 0;
    
    public static void main(String[] args) {
        int[] tab = new int[args.length];

        convertToIntArray(args, tab);

        for (int i : tab) {
            System.out.println(i);
        }

        System.out.println("Il y a " + countNumberOfEven(tab) + " nombre pair dans le tableau");

        System.out.println("Le plus gros entier du tableau est : " + getBiggestInt(tab, tab.length));
    }

    private static int[] convertToIntArray(String[] args, int[] tab) {
        if (i < args.length) {
            tab[i] = Integer.valueOf(args[i]);
            i++;
            convertToIntArray(args, tab);
        }
        i = 0;

        return tab;
    }

    private static int countNumberOfEven(int[] tab) {
        if (i < tab.length) {
            if ((tab[i] & 1) == 0) {
                i++;
                return 1 + countNumberOfEven(tab);
            } else {
                i++;
                return 0 + countNumberOfEven(tab);
            }
        }
        return 0;
    }

    private static int getBiggestInt(int[] tab, int n) {
        if (n == 1) {
            return tab[0];
        } else {
            return Math.max(tab[n-1], getBiggestInt(tab, n-1));
        }
    }
}
