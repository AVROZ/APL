import java.lang.Thread;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Traces {
    public static void main(String[] args) {
        Map<Thread, StackTraceElement[]> map = Thread.getAllStackTraces();
        Set<Thread> set = map.keySet();
        Iterator<Thread> it = set.iterator();

        while (it.hasNext()) {
            Thread key = it.next();
            System.out.println("Trace information for the thread " + key);
            StackTraceElement[] trace = map.get(key);

            for(int i = 0; i < trace.length; i++){
                System.out.println(trace[i]);
            }
            System.out.println();
        }
    }
}