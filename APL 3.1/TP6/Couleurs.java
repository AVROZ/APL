import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Couleurs {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        JPanel leftPanel = new JPanel();
        JPanel rightPanel = new JPanel();

        HashMap<String, Color> map = new HashMap<>();
        JList<String> liste = new JList<>();
        String ligne = new String();
        String[] fields;

        frame.setSize(300, 300);
        frame.setLocation(100, 100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        try {
            BufferedReader lecture = new BufferedReader(
                                     new FileReader("rgb.txt"));
            try {
                while((ligne = lecture.readLine()) != null) {
                    fields = ligne.trim().split("\\s+");
                    map.put(fields[3], new Color(Integer.valueOf(fields[0]), Integer.valueOf(fields[1]), Integer.valueOf(fields[2])));
                }
            } catch(IOException e) {
                System.err.println("Erreur de lecture dans rgb.txt !");
            }
            try {
                lecture.close();
            } catch(IOException e) {
                System.err.println("Erreur de fermeture de rgb.txt !");
            }
        } catch(FileNotFoundException e) {
            System.err.println("Erreur d'ouverture de rgb.txt !");
        }
        
        Set<String> set = map.keySet();
        Iterator<String> it = set.iterator();

        while (it.hasNext()) {
            liste.add(new JTextField(it.next()));   // Ajouter la string dans un composant graphique
        }

        leftPanel.add(liste);

        frame.add(leftPanel, BorderLayout.WEST);
        frame.add(rightPanel, BorderLayout.EAST);
        frame.setVisible(true);
    }
}