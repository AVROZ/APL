import java.util.LinkedList;

public class Chaine<E> implements Pile<E> {
    private E element;
    private LinkedList<E> chainList;

    public Chaine() {
        chainList = new LinkedList<>();
    }

    public void push(E element) {
        chainList.push(element);
    }

    public E pop() {
        return chainList.pop();
    }
}
