public interface Pile<E> {

    public void push(E element);

    public E pop();

    // public void isEmpty();
}
