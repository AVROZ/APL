import java.util.ArrayDeque;

/**
 * Arithmetique
 */
public class ArithmetiqueV2 {

    private static int valeur1;
    private static int valeur2;

    public static void main(String[] args) {
		ArrayDeque<Integer> pile = new ArrayDeque<>(args.length);
        Chaine<Integer> chaine = new Chaine<>();
		
		for (int i = 0; i < args.length; i++) {
            if (isStringInt(args[i])) {
                chaine.push(Integer.valueOf(args[i]));
            } else {
                valeur1 = chaine.pop();
                valeur2 = chaine.pop();
                try {
                    chaine.push(makeOperation(args[i], valeur1, valeur2));
                } catch (Exception e) {
                    System.err.println("Impossible d'effectuer l'opération");
                }
            }
        }

        // Affichage
        System.out.println("= " + String.valueOf(chaine.pop()));
    }

    public static boolean isStringInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static Integer makeOperation(String sign, int valeur1, int valeur2) throws Exception {
        if (sign.equals("+")) {
            return valeur2+valeur1;
        }
        if (sign.equals("-")) {
            return valeur2-valeur1;
        }
        if (sign.equals("x")) {
            return valeur2*valeur1;
        }
        if (sign.equals("/")) {
            return valeur2/valeur1;
        } else {
            throw new Exception();
        }
    }
}